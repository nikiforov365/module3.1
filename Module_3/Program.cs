using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Module_3
{
    class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
        }
    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            bool result = int.TryParse(source, out int num);
            if (!result)
            {
                throw new ArgumentException($"Uncorrect input: {source}.");
            }
            return num;
        }

        public int Multiplication(int num1, int num2)
        {
            int result = 0;
            if (num1 != 0 && num2 != 0)
            {
                result = num1 * num2;
            }
            else
            {
                result = 0;
            }
            return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool ans = int.TryParse(input, out result);
            if (ans && result<0)
            {
                ans = false;
                Console.WriteLine("Uncorrect input, try again.");
            }
            return ans;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> result = new List<int>();
            for(int i=0; i<=naturalNumber;i++)
            {
                result.Add(i);
            }
            return result;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool ans = int.TryParse(input, out result);
            if (ans && result < 0)
            {
                ans = false;
                Console.WriteLine("Uncorrect input, try again.");
            }
            return ans;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            Regex regex = new Regex(@"8");
            string result = regex.Replace(source.ToString(), "");
            return result;
        }
    }
}
